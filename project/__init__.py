# Imports
from flask import Flask

## Config
#app = Flask(__name__)
#print(app)
#from . import views

# Configuration with Blueprint
app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

# Blueprints in the project
from project.users.views import users_blueprint

# Register the blueprints
app.register_blueprint(users_blueprint)
